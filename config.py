class Config(object):
    DEBUG = True
    TESTING = False
    SQLALCHEMY_DATABASE_URI = "postgresql://postgres:@db:5432/jorrvaskr"
    SQLALCHEMY_TRACK_MODIFICATIONS = False
